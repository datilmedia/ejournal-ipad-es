ventanaArchivo = Titanium.UI.currentWindow;

var PageFlip = require('ti.pageflip');
var datos = [];
var tituloArchivo = [];
var ubicacionArchivo = [];
var fechaArchivo = [];
var archivoCerrar = Ti.UI.createButton({
	top: 10,
	width: 200,
	left: 10,
	height: 40,
	backgroundImage:'../boton_azul.png',
	title: 'Cerrar'
});	
var botonRegresar = Titanium.UI.createButton({
	title: 'Regresar',
	backgroundImage:'../boton_azul.png',
	height: 40,
	left: 10,
	width: 200,
	top: 10,
});
	
var db = Titanium.Database.open('spurrierdb');
db.execute("CREATE TABLE IF NOT EXISTS SPURRIER (TITULO TEXT, NOMBRE_ARCHIVO TEXT, FECHA TEXT)");
var archivos = db.execute('SELECT * FROM SPURRIER');

var contador = 0;
var numeroTotalDeFilas = archivos.getRowCount()

while(archivos.isValidRow()) {
	tituloArchivo.push({
		title : archivos.fieldByName('titulo')
	});
	ubicacionArchivo.push({
		ubicacion : archivos.fieldByName('nombre_archivo')
	})
	fechaArchivo.push({
		fecha : archivos.fieldByName('fecha')
	})
	archivos.next();
}

archivos.close();
db.close();

for (var c=0;c<tituloArchivo.length;c++){
	var bgcolor = (c % 2) == 0 ? '#fff' : '##dff2ff';
	
	var filaDeArchivo = Ti.UI.createTableViewRow({
		hasChild: false,
		height: 40,
		backgroundColor: bgcolor
	});
	
	var vistaFila = Ti.UI.createView({
		height: '100%',
		layout: 'vertical',
		left: 5,
		top: 5,
		bottom: 5,
		right: 5
	});

	var etiquetaDeTitulo = Ti.UI.createLabel({
		text: tituloArchivo[c].title,
		left: 54,
		width: 600,
		top: 5,
		bottom: 2,
		height: 16,
		textAlign: 'left',
		color: '#444444',
		font: {
			fontFamily:'Trebuchet MS',
			fontSize:18,
			fontWeight:'bold'
		}
	});
	
	vistaFila.add(etiquetaDeTitulo);

	var etiquetaDeFecha = Ti.UI.createLabel({
		text: fechaArchivo[c].fecha,
		left: 54,
		width: 120,
		top: 0,
		bottom: 2,
		height: 16,
		textAlign: 'left',
		color: '#444444',
		font: {
			fontFamily:'Trebuchet MS',
			fontSize:16,
		}
	});
	
	vistaFila.add(etiquetaDeFecha);
	filaDeArchivo.add(vistaFila)
	datos[c] = filaDeArchivo;
}

var tablaDeArchivos = Titanium.UI.createTableView({
	data: datos,
	editable: true,
	deleteButtonTitle: 'Eliminar',
	minRowHeight: 58,
	top: 60
});

ventanaArchivo.add(tablaDeArchivos);

if (Ti.App.Properties.getString('lenguaje') == 'es') {
	tablaDeArchivos.deleteButtonTitle = "Eliminar";
} else {
	tablaDeArchivos.deleteButtonTitle = "Delete";
};

ventanaArchivo.addEventListener('delete',function(e) {
	var db = Titanium.Database.open('spurrierdb');
	var hola = fechaArchivo[e.index].fecha + '.pdf';
	db.execute('DELETE FROM SPURRIER WHERE FECHA = ?',fechaArchivo[e.index].fecha);
	db.close();
	Ti.API.info(fechaArchivo[e.index].fecha + '.pdf'); 
	var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,fechaArchivo[e.index].fecha + '.pdf');
	if (file.exists()) { 
		file.deleteFile() 
	}
});

tablaDeArchivos.addEventListener('click', function(e) {
	Titanium.UI.setBackgroundColor('#000');
	var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, ubicacionArchivo[e.index].ubicacion);
	Ti.API.info("Nombre del archivo: "+ubicacionArchivo[e.index].ubicacion);
	var webview = Ti.UI.createWebView({
		data: file.read(),
		top: 60,
		width: '100%',
	});

	/********************PAGE FLIP STARTS******************************************/
	
	var scrollViewPdf = Ti.UI.createScrollView({			
		zoomScale: 1.0,
		minZoomScale: 1.0,
		maxZoomScale: 4.0,
		top: 60
	});
	
	var pageflip = PageFlip.createView({
	    /* All Options: TRANSITION_FLIP [default], TRANSITION_SLIDE, TRANSITION_FADE, TRANSITION_CURL */
		transition: PageFlip.TRANSITION_CURL,
		transitionDuration: 1,
		pdf: file.nativePath,
		tapToAdvanceWidth: '15%',
		landscapeShowsTwoPages: true
	});
								
	scrollViewPdf.add(pageflip);	
	
	scrollViewPdf.addEventListener('doubletap', function(){
		scrollViewPdf.zoomScale = 1.0;
	});
	
	pageflip.addEventListener('change', function(evt) {
		scrollViewPdf.zoomScale = 1.0;
	});
	
	/*****************************PAGE FLIP ENDS****************************************/	
	
	var ventanaDeLectura = Titanium.UI.createWindow({
		backgroundColor: 'white',
		width: '100%',
		height: '100%'
	});
	
	var vistaBotonesLectura = Ti.UI.createView({
		borderColor: '#000',
		borderWidth: 1,
		borderRadius: 1,
		backgroundColor: 'white',
		width: '100%',
		height: 60,
		top: '0',
		left: 0
	});
	
	vistaBotonesLectura.add(botonRegresar);
	
	botonRegresar.addEventListener('click', function() {
		ventanaDeLectura.close({
			opacity: 0,
			duration: 500
		});
	});
	
	ventanaDeLectura.add(vistaBotonesLectura);
	ventanaDeLectura.add(scrollViewPdf);
	ventanaDeLectura.open({transition:Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT});
});

archivoCerrar.addEventListener('click', function() {
	ventanaArchivo.close({
		opacity:0,
		duration:500
	});
});

ventanaArchivo.add(archivoCerrar);
ventanaArchivo.open();
