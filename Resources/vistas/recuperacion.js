ventanaRecuperacion = Titanium.UI.currentWindow;

require('passwordReset');

ventanaRecuperacion.setBackgroundGradient( {
	type : 'linear',
	colors : ['#00477F','#0084F0'],
	backFillStart : false
});

var detalle;

var image = Ti.UI.createImageView({
	image : '../logo.png',
	width : 254,
	height : 78,
	top : '30%',
});

var username = Ti.UI.createTextField({
	autocapitalization : Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	width : 300,
	top : '40%',
	height : 35,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
});

var buttonCancelar = Titanium.UI.createButton({
	backgroundImage : '../boton_azul.png',
	top :'54.5%',
	left : '10%',
	width : 300,
	height : 40,
});

var buttonRecuperar = Titanium.UI.createButton({
	backgroundImage : '../boton_azul.png',
	top : '54.5%',
	right : '10%',
	width : 300,
	height : 40,
});

var textoAyudaRecuperacion = Ti.UI.createLabel({
	text : detalle,
	width : '60%',
	height : '20%',
	color : 'white',
	textAlign : 'center',
	top : '38.5%',
	font : {
		fontSize : 16,
		fontWeight : 'bold'
	}
});

ventanaRecuperacion.add(textoAyudaRecuperacion);
ventanaRecuperacion.add(image);
ventanaRecuperacion.add(username);
ventanaRecuperacion.add(buttonCancelar);
ventanaRecuperacion.add(buttonRecuperar);

/*********Mensajes de recuperacion*********/
var fondoMensajeRecuperacion = Ti.UI.createView({
    backgroundColor: 'black',
    opacity: 0.75,
    height: '100%',
    width: '100%'
});

var ventanaMensajeRecuperacion = Ti.UI.createView({
	borderWidth : 5,
	backgroundColor:'black',
	borderRadius : 2,
	borderColor : '#666',
    height: 180,
    width: 500
})
	
var mensajeRecuperacionSuccess = Ti.UI.createLabel({
	text : '',
	width : '70%',
	top : '28%',
	height : '20%',
	textAlign : 'center',
	color : 'white',
	font : {
		fontFamily : 'Trebuchet MS',
		fontSize : '16%',
		fontWeight : 'bold'
	}
});

var mensajeRecuperacionError = Ti.UI.createLabel({
	text : '',
	width : '70%',
	top : '28%',
	height : '20%',
	textAlign : 'center',
	color : 'white',
	font : {
		fontFamily : 'Trebuchet MS',
		fontSize : '16%',
		fontWeight : 'bold'
	}
});
	
var aceptarRecuperacion = Ti.UI.createButton({
	width : '40%',
	height : '20%',
	backgroundImage:'../boton_azul.png',
	title : '',
	top : '70%',
	font : {
	fontFamily : 'Trebuchet MS',
	fontWeight : 'bold'
	}
});	

ventanaMensajeRecuperacion.add(aceptarRecuperacion);
fondoMensajeRecuperacion.add(ventanaMensajeRecuperacion);

var cargandoIngreso = Ti.UI.createView({
    backgroundColor: 'black',
	opacity: 0.75,
	height: '100%',
	width: '100%'
});
 
var indicadorDeIngreso = Ti.UI.createActivityIndicator({
    style: Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
    font : {
    	font : 'Arial',
		fontSize : 24
	},
	color: '#FFF'
});
cargandoIngreso.add(indicadorDeIngreso);

if(getOrientation(Titanium.Gesture.orientation) == 1 || getOrientation(Titanium.Gesture.orientation) == 2){
	image.top = '30%';
	username.top ='40%';
	
	buttonCancelar.top = '54.5%';
	buttonRecuperar.top = '54.5%';
	
	textoAyudaRecuperacion.top = '38.5%';
}

if(getOrientation(Titanium.Gesture.orientation) == 3 || getOrientation(Titanium.Gesture.orientation) == 4){
	image.top = '30%';
	username.top ='44%';
	
	buttonCancelar.top = '60%';
	buttonCancelar.left = '20%';
	
	buttonRecuperar.top = '60%';
	buttonRecuperar.right = '20%';
	
	textoAyudaRecuperacion.top = '43.7%';
}

Titanium.Gesture.addEventListener('orientationchange', function(e){
if(getOrientation(Titanium.Gesture.orientation) == 1 || getOrientation(Titanium.Gesture.orientation) == 2){
	image.top = '30%';
	username.top ='40%';
	
	buttonCancelar.top = '54.5%';
	buttonCancelar.left = '10%';
	buttonRecuperar.top = '54.5%';
	buttonRecuperar.right = '10%';
	
	textoAyudaRecuperacion.top = '38.5%';
}
if(getOrientation(Titanium.Gesture.orientation) == 3 || getOrientation(Titanium.Gesture.orientation) == 4){
	image.top = '30%';
	username.top ='44%';
	
	buttonCancelar.top = '60%';
	buttonCancelar.left = '20%';
	
	buttonRecuperar.top = '60%';
	buttonRecuperar.right = '20%';

	
	
	textoAyudaRecuperacion.top = '43.7%';
}
});

function getOrientation(o)
{  
	switch (o)
	{
		case Titanium.UI.PORTRAIT:
			return '1';
		case Titanium.UI.UPSIDE_PORTRAIT:
			return '2';
		case Titanium.UI.LANDSCAPE_LEFT:
			return '3';
		case Titanium.UI.LANDSCAPE_RIGHT:
			return '4';
		case Titanium.UI.FACE_UP:
			return 'face up';
		case Titanium.UI.FACE_DOWN:
			return 'face down';
		case Titanium.UI.UNKNOWN:
			return 'unknown';
	}
}

function CurrentOrientation(e)
{
    var w = Titanium.Platform.displayCaps.platformWidth;
    var h = Titanium.Platform.displayCaps.platformHeight;
    if( w > h){
        return 2;
    }else{
        return 1;
    }
};

ventanaRecuperacion.addEventListener('focus', function(e){

	if(Ti.App.Properties.getString('lenguaje') == 'es'){
		username.hintText = 'Email';
		buttonCancelar.title = 'Cancelar';
		buttonRecuperar.title = 'Recuperar';
		textoAyudaRecuperacion.text = 'Ingrese su nombre de usuario y presione en el botón "Recuperar". Recibirá un correo electrónico con las indicaciones para recuperar su contraseña.';
		mensajeRecuperacionSuccess.text = 'Se ha enviado un correo electrónico con las instrucciones para recuperar su contraseña.';
		mensajeRecuperacionError.text = 'El correo electrónico no existe en nuestro sistema, por favor contacte a Grupo Spurrier.';
		aceptarRecuperacion.title = 'Aceptar';
		
	}else{
		username.hintText = 'Email';
		buttonCancelar.title = 'Cancel';
		buttonRecuperar.title = 'Recover';
		textoAyudaRecuperacion.text = 'Type your username and press "Recover". You\'re going to recieve an email with the instructions to recover your password.'
		mensajeRecuperacionSuccess.text = 'An email was sent to you with the instructions to reset your password';
		mensajeRecuperacionError.text = 'The email was not found in out system. Please, contact Grupo Spurrier.';
		aceptarRecuperacion.title = 'Ok';
		
	};	
	
	if(getOrientation(Titanium.Gesture.orientation) == 1 || getOrientation(Titanium.Gesture.orientation) == 2){
	image.top = '30%';
	username.top ='40%';
	
	buttonCancelar.top = '54.5%';
	buttonRecuperar.top = '54.5%';
	
	textoAyudaRecuperacion.top = '38.5%';
	}
	
	if(getOrientation(Titanium.Gesture.orientation) == 3 || getOrientation(Titanium.Gesture.orientation) == 4){
		image.top = '30%';
		username.top ='44%';
		
		buttonCancelar.top = '60%';
		buttonCancelar.left = '20%';
		
		buttonRecuperar.top = '60%';
		buttonRecuperar.right = '20%';
		
		textoAyudaRecuperacion.top = '43.7%';
	}

		
});

buttonRecuperar.addEventListener('click', function()
{
	ventanaRecuperacion.add(cargandoIngreso);
	indicadorDeIngreso.show();
		
	Parse.initialize("EmAchlYLvbIbDdUqnyjO93ZM9eZya8whcpNqkbRT", "HrD48N5DJb4gz7eKLhmyzb8SfsUbe5UepMbl42vx");	

	Parse.User.requestPasswordReset(username.value, {
		success: function(data) {
			ventanaRecuperacion.remove(cargandoIngreso);
			ventanaMensajeRecuperacion.add(mensajeRecuperacionSuccess);
			ventanaRecuperacion.add(fondoMensajeRecuperacion);
	  	},
	  	error: function(error) {
	  		ventanaRecuperacion.remove(cargandoIngreso);
			ventanaMensajeRecuperacion.add(mensajeRecuperacionError);
			ventanaRecuperacion.add(fondoMensajeRecuperacion);
		}
	});

});

aceptarRecuperacion.addEventListener('click', function()
{
	ventanaMensajeRecuperacion.remove(mensajeRecuperacionSuccess);
	ventanaMensajeRecuperacion.remove(mensajeRecuperacionError);
	ventanaRecuperacion.remove(fondoMensajeRecuperacion)
});

buttonCancelar.addEventListener('click', function()
{
	ventanaRecuperacion.close({view:ventanaRecuperacion,opacity:0,duration:500});
});
