var parse = require('parselogin');
var parseCloud = require('parsecloud');

parseCloudClient = new parseCloud.Client();

if( Ti.App.Properties.hasProperty( "lenguaje" )==0) {
	lenguaje = 'es';
	Ti.App.Properties.setString('lenguaje', lenguaje);
};

var textoAlert;

var image = Ti.UI.createImageView({
	image: 'logo.png',
	width: 254,
	height: 78,
	top: '20%',
});

var logoGrupoSpurrier = Ti.UI.createImageView({
	image: 'logo-gs.png',
	width: 200,
	height: 43,
	bottom: '45',
	left: '40'
});

var username = Ti.UI.createTextField({
	autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	width: 300,
	top: '30%',
	height: 35,
	borderStyle: Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
});

var password = Ti.UI.createTextField({
	autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	width: 300,
	top: '34%',
	height: 35,
	borderStyle: Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	passwordMask: true,
});

var buttonEntrar = Titanium.UI.createButton({
	backgroundImage: 'boton_azul.png',
	top: '38%',
	width: 300,
	height: 40,
});

var buttonRecuperacion = Titanium.UI.createButton({
	backgroundImage: 'boton_azul.png',
	top: '65%',
	width: 300,
	height: 40,
});

var buttonSuscripcion = Titanium.UI.createButton({
	backgroundImage: 'boton_azul.png',
	top: '60%',
	width: 300,
	height: 40,
});

var buttonRegresar = Titanium.UI.createButton({
	title: 'Cerrar',
	backgroundImage:'boton_azul.png',
	height: 40,
	left: 10,
	width: 200,
	top: 10,
});

var labeleditores = Titanium.UI.createLabel({
	color: 'ffffff',
	id: 'font_label_test',
	text: 'Walter Spurrier Baquerizo \n DIRECTOR \n\n Alberto Acosta Burneo \n EDITOR',
	top: '43%',
	height: 170,
	textAlign: 'center'
});

var labelDireccion = Titanium.UI.createLabel({
	color: 'ffffff',
	id: 'direccion',
	text: 'Plaza Lagos, Edificio Mirador, oficina 2-1 \nKm. 6,5 Vía Puntilla – Samborondón, Guayaquil \nPBX (593-4) 500 9343 \nwww.grupospurrier.com \ninfo@grupospurrier.com',
	height: 120,
	bottom: 10,
	right: 40
})

var cargandoIngreso = Ti.UI.createView({
    backgroundColor: 'black',
	opacity: 0.75,
	height: '100%',
	width: '100%'
});
 
var indicadorDeIngreso = Ti.UI.createActivityIndicator({
    style: Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
    font : {
    	font: 'Arial',
		fontSize: 24
	},
	color: '#FFF'
});

cargandoIngreso.add(indicadorDeIngreso);

var win = Titanium.UI.createWindow({  
	title: 'ZipFile test',
	backgroundColor: 'transparent',
});

win.orientationModes = [ 
    Titanium.UI.PORTRAIT
];

win.setBackgroundGradient( {
	type: 'linear',
	colors: ['#10427a','#305790'],
	backFillStart: false
});

var errorIngreso = Ti.UI.createView({
    backgroundColor: 'black',
    opacity: 0.75,
    height: '100%',
    width: '100%'
});

var mensajeErrorIngreso = Ti.UI.createView({
	borderWidth: 5,
	backgroundColor: 'black',
	borderRadius: 2,
	borderColor: '#666',
   // opacity: 0.75,
    height: 150,
    width: 400
})
	
var textoErrorIngreso = Ti.UI.createLabel({
	text: '',
	width: '70%',
	top: '28%',
	height: '20%',
	textAlign: 'center',
	color: 'white',
	font: {
		fontFamily: 'Trebuchet MS',
		fontSize: '16%',
		fontWeight: 'bold'
	}
});
	
var aceptarErrorIngreso = Ti.UI.createButton({
	width: '40%',
	height: '20%',
	backgroundImage: 'boton_azul.png',
	title: '',
	top: '70%',
	font: {
		fontFamily: 'Trebuchet MS',
		fontWeight: 'bold'
	}
});	

mensajeErrorIngreso.add(textoErrorIngreso);
mensajeErrorIngreso.add(aceptarErrorIngreso);
errorIngreso.add(mensajeErrorIngreso);

win.addEventListener('focus', function(e) {
	if (Ti.App.Properties.getString('lenguaje') == 'es') {
		username.hintText = 'Nombre de Usuario';
		password.hintText = 'Contraseña';
		buttonEntrar.title = 'Entrar';
		buttonRecuperacion.title = 'Cambie/Recupere su Contraseña';
		indicadorDeIngreso.message = 'Ingresando, espere por favor...';
		textoErrorIngreso.text = 'Usuario o contraseña no válidos';
		aceptarErrorIngreso.title = 'Aceptar';
		buttonSuscripcion.title = 'Suscríbase';
	} else {
		username.hintText = 'Username';
		password.hintText = 'Password';
		buttonEntrar.title = 'Login';
		buttonRecuperacion.title = 'Change/Recover your password';
		indicadorDeIngreso.message = 'Logging in, please wait...';
		textoErrorIngreso.text = 'Invalid username or password';
		aceptarErrorIngreso.title = 'Ok';
		buttonSuscripcion.title = 'Subscribe';
	};
});

win.add(image);
win.add(logoGrupoSpurrier);
win.add(username);
win.add(password);
win.add(buttonEntrar);
win.add(labeleditores);
win.add(labelDireccion);
//win.add(buttonSuscripcion);
win.add(buttonRecuperacion);

aceptarErrorIngreso.addEventListener('click', function() {
	win.remove(errorIngreso);
});

buttonRecuperacion.addEventListener('click', function() {
	var ventanaRecuperacion = Titanium.UI.createWindow({
		url: 'vistas/recuperacion.js',
		title: 'recuperacion',
		backgroundColor: 'white',
		width: '100%',
		height: '100%',
	});
	
	ventanaRecuperacion.open({
		transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT
	});
});

buttonSuscripcion.addEventListener('click', function() {
	var urlSuscripcion = 'http://grupospurrier.com/sp/html/servicios/analisis/suscripciones/index.php';
	
	var webview = Titanium.UI.createWebView({
		url: urlSuscripcion,
		width: '100%',
		top: 60
	});
	
    var window = Titanium.UI.createWindow({
    	width: '100%',
    	height: '100%'
    });
	
	buttonRegresar.addEventListener('click', function() {
		window.close({
			opacity: 0,
			duration: 500
		});
	});
	
	var vistaBotonVolver = Ti.UI.createView({
		borderColor: '#000',
		borderWidth: 1,
		borderRadius: 1,
		backgroundColor: 'white',
		width: '100%',
		height: 60,
		top: '0',
		left: 0
	});
	
	vistaBotonVolver.add(buttonRegresar);
	window.add(vistaBotonVolver);
	window.add(webview);
	window.open({
		transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
	});
});

buttonEntrar.addEventListener('click', function() {
	win.add(cargandoIngreso);
	indicadorDeIngreso.show();
	
	var ventanaInicio = Titanium.UI.createWindow({
		url: 'vistas/inicio.js',
		title: 'inicio',
		backgroundColor: 'white',
		width: '100%',
		height: '100%',
	});
	
	var client = new parse.Client();
/*	
	var whereClause = {
		"objectId" : "WG4n4qiZBK"
	};*/
	client.get({
		className: 'Editions',
		payload: {
			"username" : username.value,
			"password" : password.value
		},
		success: function(user) {
			if (user.userAccessQuota > 0) {
				saveParams = {
					className: 'User', objectId: user.objectId,
					object: {'userAccessQuota': {'__op': "Increment", 'amount': -1}},
					sessionToken: user.sessionToken,
					success: function(savedUser) {
						win.remove(cargandoIngreso);
						ventanaInicio.open({transition:Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT});
						indicadorDeIngreso.hide();
					},
					error: function(savedUser, error) {
						console.log('Error al administrar cupos de acceso.');
					}
				};
				parseCloudClient.update(saveParams);
			} else {
				alert('Ya no tiene accesos semanales disponibles. / You no longer have access permissions available.');
				win.remove(cargandoIngreso);
				win.add(errorIngreso);
			}
		},
		error: function(response, xhr) {
			win.remove(cargandoIngreso);
			win.add(errorIngreso);
		}
	});
});

win.open();
